provider "aws" {
    region = "ap-south-1"
}

module "project_tf" {
    source = "./modules/IaaC"

    vpc_cidr = "10.0.0.0/16"
    public_cidr_block = "10.0.1.0/24"
    private_cidr_block = "10.0.2.0/24" 
    public_az = "ap-south-1a"
    private_az = "ap-south-1a"
}

module "project_tf_1" {
    source = "./modules/IaaC"

    vpc_cidr = "192.168.0.0/16"
    public_cidr_block = "192.168.1.0/24"
    private_cidr_block = "192.168.2.0/24" 
    public_az = "ap-south-1a"
    private_az = "ap-south-1a"
}

module "project_tf_2" {
    source = "./modules/IaaC"

    vpc_cidr = "172.31.0.0/16"
    public_cidr_block = "172.31.1.0/24"
    private_cidr_block = "172.31.2.0/24" 
    public_az = "ap-south-1a"
    private_az = "ap-south-1a"
}

module "project_tf_3" {
    source = "./modules/IaaC"

    vpc_cidr = "172.31.0.0/16"
    public_cidr_block = "172.31.1.0/24"
    private_cidr_block = "172.31.2.0/24" 
    public_az = "ap-south-1a"
    private_az = "ap-south-1a"
}