## Define vpc

resource "aws_vpc" "project-vpc" {
    cidr_block = var.vpc_cidr

    tags = {Name = "project-tf VPC"}
}

## define Public Subnet
resource "aws_subnet" "project-public-subnet" {
    vpc_id = aws_vpc.project-vpc.id
    cidr_block = var.public_cidr_block
    availability_zone = var.public_az
}

## define Private Subnet
resource "aws_subnet" "project-private-subnet" {
    vpc_id = aws_vpc.project-vpc.id
    cidr_block = var.private_cidr_block
    availability_zone = var.private_az
}

