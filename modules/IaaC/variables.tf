## VPC variables

variable "vpc_cidr" {
    description = "CIDR for VPC"
    default = "10.0.0.0/16"
}

variable "public_cidr_block" {
    description = "CIDR for VPC public subnet"
}

variable "public_az" {
    description = "AZ for VPC public subnet"
}

variable "private_cidr_block" {
    description = "CIDR for VPC private subnet"
}

variable "private_az" {
    description = "AZ for VPC private subnet"
}
