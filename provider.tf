terraform {
    backend "s3" {
        bucket = "terraform-11am-backend"
        key    = "tfstate/terraform.tfstate"
        region = "us-east-1"
        dynamodb_table = "terraform-11am-backend-table"
        profile = "default"
    }
}